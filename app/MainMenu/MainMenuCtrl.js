'use strict';

angular
    .module('myApp')
    .controller('MainMenuCtrl', function() {

        var vm = this;

        vm.list = [
            {
                sref: 'list',
                text: 'Список'
            },
            {
                sref: 'createNewPokemon',
                text: 'Добавить нового'
            }
        ]

    });